import React, { Component } from 'react';
import Login from './login/Login';
import { Route, Switch, Redirect} from 'react-router-dom';
import './App.css';
import PrivateRoute from './utils/PrivateRoute';
import Layout from './hoc/Layout/Layout';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import myCalendar from './calendar/Calendar';
import Logout from './logout/Logout';

class App extends Component {

  render () {
    return (
      <div className="App">
        <ReactNotification />
        <Layout>
        <Switch>
                    <Route path="/login" component={Login} />
                    <PrivateRoute path="/calendar" component={myCalendar} />
                    <PrivateRoute path="/logout" component={Logout} />
                    <Redirect exact from="/" to="/calendar" />
                    <Route render={() => <h1>Not found</h1>}/>
        </Switch>
        </Layout>
      </div>
    );

  }  
}

export default App;
