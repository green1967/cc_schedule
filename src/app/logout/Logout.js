import React, {Component} from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookies';

class Logout extends Component {

    render() {

        localStorage.removeItem(this.props.user_id);
        cookie.remove('token');
        window.location = "/login";

        return(
            <div></div>
            )

        }
}

const mapStateToProps = state => {
    return {
        user_id: state.user.id,
    };
};

export default connect(mapStateToProps)(Logout);