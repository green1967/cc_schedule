import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import './eventform.css';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import TimePicker from 'react-time-picker';


const EventForm = (props) => {
    const [validated, setValidated] = useState(false);
  
    const handleSubmit = (event) => {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
   
    setValidated(true);
    event.preventDefault();
    if(form.checkValidity() === true)  props.formSubmit();
      
    };
  
    return (
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group controlId="Title">
                    <Form.Control
                        required 
                        type="text" 
                        placeholder="Event name" 
                        name='title' 
                        value={props.set.title} 
                        onChange={props.handleChange}/>                    
                </Form.Group>
                <Form.Group>
                <TimePicker
                    onChange={props.handleDateChange}
                    name="startDate"
                    value={new Date(props.set.startDate)}
                    minTime=''  
                    />
                </Form.Group>
                <Form.Group controlId="Duration">
                    <Form.Control 
                        required
                        type="text" 
                        placeholder="Duration"
                        name="duration"                         
                        value={props.set.duration} 
                        onChange={props.handleChange}/>                   
                </Form.Group>
                <ButtonToolbar>
                <Button type="submit" className='loginBtn'>
                    Save
                </Button>                
                {props.set._id && <Button variant="secondary" onClick = {props.onDeleteClick}>
                    Delete
                </Button>}
                </ButtonToolbar> 
        </Form>
    )
}

export default EventForm;