import React from 'react';

const Event=(props) => (
    <div style={props.style} onClick = {props.onClick}>
    <div className="task-box bg-color cursor-pointer">
    <div className="text-box">
        <div className="text-box-width">
        <div className="text-style">{props.title}</div>
        </div>
    </div>
    </div>
    </div>
)

export default Event;