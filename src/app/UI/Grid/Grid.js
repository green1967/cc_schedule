import React, {Fragment} from 'react';
import Events from '../../events/Events';
import './grid.css';

const config = require('../../../config.json');

const Grid = (props) => {   


return (
<Fragment>
<div style = {{width: "100%"}}>
    <div style={{display: "flex"}}>
        <div className="time-top-block"></div>
        <div  className="lines-top-block">
            <div className='date-cell'>
                <div className='week-day'>Fri</div>
                <div className='month-day'>12</div>
            </div>
        </div>
    </div>
    <div style={{display: "flex"}}>
        <div className="time-block">
            <table style={{width: "80px"}}>
                <tbody>
                    <tr>
                        <td>
                            <div style={{height: "28px"}}></div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>8:30 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>9:00 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>9:30 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>10:00 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>10:30 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>11:00 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>11:30 AM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>12:00 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>12:30 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>13:00 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>13:30 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>14:00 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>14:30 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>15:00 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>15:30 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>16:00 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-small">
                                <span>16:30 PM</span>
                            </div>
                            <div className="time-cell font-time-cell-common font-large">
                                <span>17:00 PM</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div className="lines-block border-grey">
            <table style={{width: "100%", borderCollapse: "collapse"}}>
                <tbody>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T08:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T08:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T09:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T09:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T10:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T10:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T11:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T11:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line"onClick={()=>props.onClick({startDate: config.defaultDate+'T12:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T12:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T13:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T13:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T14:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T14:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T15:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T15:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line" onClick={()=>props.onClick({startDate: config.defaultDate+'T16:00:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                    <tr>
                        <td className="time-line border-bottom-grey" onClick={()=>props.onClick({startDate: config.defaultDate+'T16:30:00+03:00', duration: config.defaultDuration})}></td>
                    </tr>
                </tbody>
            </table>
            <div style={{position: "absolute", width: "100%",  top: "0", left: "0"}}>
            <Events onClick={(event) => props.onClick(event)}/>
            </div>
        </div>
        
    </div>
</div>
</Fragment>
)
}

export default Grid;