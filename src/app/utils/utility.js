import axios from 'axios';
import { createBrowserHistory } from 'history';


export const history = createBrowserHistory();

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const axios_i = axios.create({
    baseURL: 'http://member.gbox.pp.ua:4040/',
});
