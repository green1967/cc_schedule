import React, {Component} from 'react';
import actions from '../login/duck/actions';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import cookie from 'react-cookies';


class PrivateRoute extends Component {
    render() {
    const user = JSON.parse(localStorage.getItem('user'));
    const expires = new Date();
    expires.setDate(Date.now() + 180000*60);    
//   console.log('cookie=', cookie.load('token'), 'user=',  user.content.email)

    let comp = user && cookie.load('token') 
    if (comp) 
    {
    cookie.save('token', user.token, { path: '/', expires, maxAge: 18000});
    this.props.loginSuccess(user._id, user.name, user.token, user.createdDate);
    comp = <Route {...this.props}/> 
    }
    else  { comp= <Redirect to='/login'/>}
    
 //   console.log('proivate_route_string=', children);
    return comp;
  }
}

  const mapDispatchToProps = dispatch => {
    return {
        loginSuccess: (id, name, token, regDate) => dispatch(actions.loginSuccess(id, name, token, regDate))
    };
};

export default connect(null, mapDispatchToProps)(PrivateRoute);