import React, {Component, Fragment} from 'react';
import eventActions from '../events/duck/actions';
import { connect } from 'react-redux';
// import './calendar.css';
import Grid from '../UI/Grid/Grid';
import SlidingPanel from 'react-sliding-side-panel';
import EventForm from '../UI/EventForm/Eventform';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';



class myCalendar extends Component {
    state = {
        event: {
            _id: '',
            user_id: '',
            calendar_id: '',
            startDate: '',
            endDate: '',
            title: '',
            duration: '',
        },
        newEvent: false,
        openPanel: false,
        openConfirmationModal: false,
        openAlertModal: false,
        alertMessage: '',
    }

componentDidMount () {
    let draftEvent = localStorage.getItem(this.props.user_id);
    if(draftEvent) {
        this.setState({alertMessage : "A draft version of event found. Do you want to continue?"});
        let draftObj = JSON.parse(draftEvent);
        this.setState({event: draftObj});
        this.alertModalOpen();
    }
            
}

openPanel = (event) => {
        if(event._id) {
        event.duration = (Date.parse(event.endDate) - Date.parse(event.startDate))/1000/60;
            this.setState({
                event: event,
                openPanel: true,
                newEvent: false,
            })
        }
        else this.setState({
            event: {
                startDate: event.startDate, 
                endDate: new Date(Date.parse(event.startDate) + event.duration*1000*60),
                duration: event.duration,
                user_id: this.props.user_id,
                calendar_id: this.props.calendar_id,
            },
            openPanel: true,
            newEvent: true,
        })

}

handleChange = (e) => {
    let { name, value } = e.target;
    if(name === 'duration') {
        let newEndDate = new Date(Date.parse(this.state.event.startDate) + value*1000*60);
 
            let newState={...this.state.event};
            newState.endDate = newEndDate;
            newState.duration = value;
            this.setState({event: newState});
        
    }
    else this.setState({event: {...this.state.event, 
                     [name]: value}});

    this.storeDraftEvent();
}

handleDateChange = (time) => {

        let newStartDate = new Date('2020-06-12T'+time+"+03:00");
        let endDate= new Date(Date.parse(newStartDate) + this.state.event.duration*1000*60);

            let newState = {...this.state.event};
            newState.startDate = newStartDate;
            newState.endDate = endDate;
            this.setState({event: newState});
            
    this.storeDraftEvent();
}

deleteHandler= () => {
    if(this.state.event._id) {
    this.props.removeEvent(this.state.event, this.props.token);
    this.setState({openPanel: false});
    }
}

formSubmit = () => {
    if(new Date (this.state.event.startDate).getHours()<8) {
        this.setState({alertMessage : "You can't add an event before 8:00 AM"});
        this.alertModalOpen();
        return 0;
    }

    if(new Date(this.state.event.endDate).getHours()>16 && new Date(this.state.event.endDate).getMinutes()>0) {
        this.setState({alertMessage : "You can't add en event with duration after 5:00 PM"});
        this.alertModalOpen();
        return 0;
    }

    if(this.state.newEvent) 
        this.props.addEvent(this.state.event, this.props.token);
    else
        this.props.updateEvent(this.state.event, this.props.token); 
    this.setState({openPanel: false});
    this.removeDraftEvent();
}

confirmationModalOpen = () => {
    this.setState({openPanel: false});
    this.setState({openConfirmationModal: true});
}

handleConfirmationClose = () => this.setState({openConfirmationModal: false});

handleConfirmationYes = () => {
    this.setState({openConfirmationModal: false});
    this.deleteHandler();
}

alertModalOpen = () => {
    this.setState({openPanel: false});
    this.setState({openAlertModal: true});
}

handleAlert = () => {
    this.setState({openAlertModal: false});
    this.setState({openPanel: true});
}

storeDraftEvent = () => localStorage.setItem(this.props.user_id, JSON.stringify(this.state.event));


removeDraftEvent = () => localStorage.removeItem(this.props.user_id);



render() {

 
return (

    <Fragment>
        <Button style={{ margin: "10px"}} onClick={() => this.props.downloadFile(this.props.user_id, this.props.calendar_id, this.props.token)} > Export to JSON </Button> 
        <Modal show={this.state.openConfirmationModal} onHide={this.handleConfirmationClose}>
                <Modal.Header closeButton>
                <Modal.Title>Alert!</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure to delete selected event?</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={this.handleConfirmationClose}>
                    No
                </Button>
                <Button variant="primary" onClick={this.handleConfirmationYes}>
                    Yes
                </Button>
                </Modal.Footer>
        </Modal>
        <Modal 
            backdrop="static"
            keyboard={false}
            show={this.state.openAlertModal} 
            onHide={this.handleAlert}>
                <Modal.Header closeButton>
                <Modal.Title>Alert!</Modal.Title>
                </Modal.Header>
                <Modal.Body>{this.state.alertMessage}</Modal.Body>
                <Modal.Footer>
                <Button variant="primary" onClick={this.handleAlert}>
                    Ok
                </Button>
                </Modal.Footer>
        </Modal>
    <div className="container content-wrap">
    <div className="activity-wrap">
        <Grid onClick={(event)=>this.openPanel(event)}/>
    </div>
    </div>
    <SlidingPanel
        type={'right'}
        isOpen={this.state.openPanel}
        backdropClicked={() => this.setState({openPanel: false})}
        size={30}
        panelClassName="additional-className"
      >
            <div className="panel-container">
                <EventForm 
                    set={this.state.event} 
                    loggingIn={this.props.loading} 
                    formSubmit={this.formSubmit} 
                    handleChange={this.handleChange}
                    handleDateChange={this.handleDateChange} 
                    onDeleteClick={this.confirmationModalOpen}
                    />
            </div>
        </SlidingPanel>        
    </Fragment>

    )
    }

};

const mapStateToProps = state => {
    return {
        token: state.user.token,
        user_id: state.user.id,
        calendar_id: state.calendar.id,
        loading: state.event.loading,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addEvent: (event, token) => dispatch(eventActions.addEvent(event, token)),
        updateEvent: (event,token) => dispatch(eventActions.updateEvent(event, token)),
        removeEvent: (event, token) => dispatch(eventActions.removeEvent(event, token)),
        downloadFile: (user_id, calendar_id, token) => dispatch(eventActions.downLoadFile(user_id, calendar_id, token)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(myCalendar);
