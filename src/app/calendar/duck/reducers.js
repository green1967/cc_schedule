import types from './types';
import { updateObject } from '../../utils/utility';

const initialState = {
    id: 13,
};


const getCalendarStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const getCalendarFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const getCalendarSuccess = ( state, action ) => {
    return updateObject( state, {
        id: action.calendar_id,
    } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.GETCALENDAR_START: return getCalendarStart( state, action );
        case types.GETCALENDAR_SUCCESS: return getCalendarSuccess( state, action );
        case types.GETCALENDAR_FAIL: return getCalendarFail( state, action )
        default: return state;
    }
};

export default reducer;