import types from './types';


const getCalendarStart = () => ({    
        type: types.GETCALENDAR_START
});

const getCalendarFail = ( error ) => ({
        type: types.GETCALENDAR_FAIL,
        error: error
});

const getCalendarSuccess = ( calendar_id ) => ({
    type: types.GETCALENDAR_SUCCESS,
    id: calendar_id
});


export default {
    getCalendarStart,
    getCalendarSuccess,
    getCalendarFail,
}