import React, { Component } from 'react';
import style from './login.module.css';
import actions from './duck/actions';
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import cookie from 'react-cookies';
import LoginForm from '../UI/Loginform/Loginform';



class Login extends Component {
    state = {
        username: '',
        password: '',
        submitted: false,
    }
    
    handleChange = (e) => {
        let { name, value } = e.target;
        this.setState({[name]: value});
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            this.props.onLoginForm(this.state.username, this.state.password);
        }
    }

    formSubmit = () => {
        const { username, password } = this.state;
        if (username && password) {
            this.props.onLoginForm(this.state.username, this.state.password);
        }
    }

    
    render ()
    {
        let authRedirect = null;
        if (cookie.load('token')) {
           authRedirect = <Redirect to='/'/>
        }

        return (
        <div>
            {authRedirect}
        <div className="container content-wrap">
            <div className={"col-12 col-sm-8 col-md-6 "+style.loginFormWrap}>        
                <h2>Login</h2>
                <LoginForm set={this.state} loggingIn={this.props.loggingIn} formSubmit={this.formSubmit} handleChange={this.handleChange} />
            </div>
        </div>
    
    </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.user.username,
        loggingIn: state.user.loading,
        token: state.user.token
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLoginForm: (username, password) => dispatch(actions.loginRequest(username, password))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Login);