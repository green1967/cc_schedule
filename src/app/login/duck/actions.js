import types from './types';
import {axios_i} from '../../utils/utility';
import cookie from 'react-cookies';
import { store } from 'react-notifications-component';
import md5 from 'md5';

const loginStart = () => ({    
        type: types.LOGIN_START
});

const loginFail = ( error ) => ({
        type: types.LOGIN_FAIL,
        error: error
});

const loginSuccess = (id, user, token, regDate) => ({
    type: types.LOGIN_SUCCESS,
    user: user,
    token: token,
    id: id,
    regDate: regDate
});

const loginRequest = ( username, password ) => {
    const expires = new Date();
    expires.setDate(Date.now() + 180000*60);

    return dispatch => {
         dispatch( loginStart() );
        axios_i.post('users/login', {name: username, password: md5(password)})
            .then( response => {
                if(response.status === 200)  {
                    const data = response.data;                    
                    localStorage.setItem('user', JSON.stringify(data)); 
                    cookie.save('token', data.token, { path: '/', expires, maxAge: 18000});
                    dispatch( loginSuccess(data._id, data.name, data.token, data.createdDate)); 
                                      
    //        });             
            } 
        })
            .catch( error => {
                let error_msg = error.response.data.message;
                            store.addNotification({
                            title: "Erorr!",
                            message: error_msg,
                            type: "danger",
                            insert: "top",
                            container: "top-right",
                            animationIn: ["animated", "fadeIn"],
                            animationOut: ["animated", "fadeOut"],
                            dismiss: {
                            duration: 3000,
                            }
                          });
                        logout();
                        dispatch( loginFail( error.response.data.message ) );
                console.log(error.response.status);                
            } );
    };
};

const logout = () => {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
 //   console.log('logout.getlocalstorage=', localStorage.getItem('user'));
}


export default {
    loginStart,
    loginSuccess,
    loginFail,
    loginRequest,
    logout
}