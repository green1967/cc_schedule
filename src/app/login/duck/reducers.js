import types from './types';
import { updateObject } from '../../utils/utility';

const initialState = {
    id: null,
    user: null,
    token: null,
    loading: false,
    regDate: null,
    expiried: false
};


const loginStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const loginFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const loginSuccess = ( state, action ) => {
    return updateObject( state, {
        id: action.id,
        user: action.user,
        token: action.token,
        loading: false,
        expired: false,
        regDate: action.regDate
    } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.LOGIN_START: return loginStart( state, action );
        case types.LOGIN_SUCCESS: return loginSuccess( state, action );
        case types.LOGIN_FAIL: return loginFail( state, action )
        default: return state;
    }
};

export default reducer;