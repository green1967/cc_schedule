const LOGIN_START = 'app/login/duck/LOGIN_START';
const LOGIN_SUCCESS = 'app/login/duck/LOGIN_SUCESS';
const LOGIN_FAIL = 'app/login/duck/LOGIN_FAIL';

export default {
    LOGIN_START,
    LOGIN_SUCCESS,
    LOGIN_FAIL
}