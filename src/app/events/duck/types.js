const GETLIST_START = 'app/events/duck/GETLIST_START';
const GETLIST_SUCCESS = 'app/events/duck/GETLIST_SUCESS';
const GETLIST_FAIL = 'app/events/duck/GETLIST_FAIL';


export default {
    GETLIST_START,
    GETLIST_SUCCESS,
    GETLIST_FAIL,    
}