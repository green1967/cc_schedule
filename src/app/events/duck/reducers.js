import types from './types';
import { updateObject } from '../../utils/utility';
import actions from './actions';

const initialState = {
    appointments: [],
    loading: false,
};


const getListStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const getListFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const getListSuccess = ( state, action ) => {
    return { ...state, 
        appointments: [...action.appointments],
        loading: false
    };
};



const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.GETLIST_START: return getListStart( state, action );
        case types.GETLIST_SUCCESS: return getListSuccess( state, action );
        case types.GETLIST_FAIL: return getListFail( state, action );
        default: return state;
    }
};

export default reducer;