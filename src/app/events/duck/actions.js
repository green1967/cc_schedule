import types from './types';
import {axios_i} from '../../utils/utility';



const getListStart = () => ({    
        type: types.GETLIST_START
});

const getListFail = ( error ) => ({
        type: types.GETLIST_FAIL,
        error: error
});

const getListSuccess = ( appointments ) => ({
    type: types.GETLIST_SUCCESS,
    appointments: appointments,
});


const getListRequest = ( user_id, calendar_id, token ) => {
    return dispatch => {
        dispatch( getListStart() );
        return axios_i.get('events/get-events-by-user',  
        {headers: {'Authorization': 'Bearer ' + token}, 
        params:{user_id: user_id, calendar_id: calendar_id}})
            .then( response => {
            if(response.status === 200)  {
                const data = response.data;
                dispatch( getListSuccess(data.events));
                 }                    
        } )
        .catch( error => {
            dispatch( getListFail( error.response.data.message ) );
        } ); 
    };
};

const addEvent = (event, token) => {
    return dispatch => {
        dispatch( getListStart() );
        return axios_i.get('events/add-event', 
                {headers: {'Authorization': 'Bearer ' + token},
                params:{user_id: event.user_id, calendar_id: event.calendar_id, 
                startDate: event.startDate, endDate: event.endDate, 
                title: event.title}})
                .then( response => {
                    if(response.status === 200)  {
                       dispatch( getListRequest(event.user_id, event.calendar_id, token));
                    }              
                } )
                .catch( error => {
                    console.log(error.response);
                    dispatch( getListFail( error.response.data.message ) );
                } );        
    }
}

const updateEvent = (event, token) => {
        return dispatch => {
           dispatch( getListStart() );
           return axios_i.get('events/'+event._id, 
               {   headers: {'Authorization': 'Bearer ' + token},
                   params: {user_id: event.user_id, calendar_id: event.calendar_id, 
                   startDate: event.startDate, endDate: event.endDate, 
                   title: event.title}})
                   .then( response => {
                       if(response.status === 200)  {
 //                        console.log('update_event.response=', response.data);
                         dispatch( getListRequest(event.user_id, event.calendar_id, token));          
                        }              
                   } )
                   .catch( error => {
                       console.log(error.response);
                       dispatch( getListFail( error.response.data.message ) );
                   } );        
       }
   }

const removeEvent = (event, token) => {
    return dispatch => {
        dispatch( getListStart() );
        return axios_i.get('/events/delete-by-id/'+event._id, 
            {   headers: {'Authorization': 'Bearer ' + token}
                })
                .then( response => {
                    if(response.status === 200)  {
                      dispatch( getListRequest(event.user_id, event.calendar_id, token));          
                     }              
                } )
                .catch( error => {
                    console.log(error.response);
                    dispatch( getListFail( error.response.data.message ) );
                } );        
    }
}

const downLoadFile = (user_id, calendar_id, token) => {
    return dispatch => {
    var fileDownload = require('js-file-download');
    return axios_i.get('events/download', 
                {headers: {'Authorization': 'Bearer ' + token},
                params:{user_id: user_id, calendar_id: calendar_id, 
                }})
                .then( response => {
                    if(response.status === 200)  {
                           fileDownload(JSON.stringify(response.data, null, 2), 'export.json'); 
                     }              
                } )
                .catch( error => {
                    dispatch( getListFail( error.response.data.message ) );
                } ); 
        } 
}


export default {
    getListStart,
    getListSuccess,
    getListFail,
    getListRequest,
    addEvent,
    updateEvent,
    removeEvent,
    downLoadFile,
}