import React, {Component, Fragment} from 'react';
import actions from './duck/actions';
import { connect } from 'react-redux';
import Event from '../UI/Event/Event';
import Spinner from '../UI/Spinner/Spinner';

const config = require('../../config.json');


class Events extends Component {
    state = {
        winHeight: 0,
        winWidth: 0,
    } 

componentDidMount () {
    this.props.onGetList(this.props.user_id, this.props.calendar_id, this.props.token); 
}

shouldComponentUpdate(nextProps, nextState) {
    if(JSON.stringify(this.props) === JSON.stringify(nextProps)) return false;
    return true;
}

removeFromArray = (original, remove) => {
    remove.map(item => original.splice(original.indexOf(item),1));
 }
  
buildTaskGrip = () => {
    const initDate = new Date(config.defaultDate + 'T' + config.dayStartTime);
    const pxPermin = 1.6;
    let  initLeftOffsetPx = 10;
    let currentWidthPx = 0;
    let initArray = [...this.props.appointments];
    let eventArray = []; 
    let showItems = [];
    let groupEvents = [];
    let eventDelList = [];
    let initDelList = [];    
    let curColumn = 0;
    let condition;

    while(initArray.length) {        
        initArray.sort((a,b) => {
            let diff1 = Date.parse(a.endDate)-Date.parse(a.startDate);
            let diff2 = Date.parse(b.endDate)-Date.parse(b.startDate);
            return diff2-diff1;
          })
          eventArray=[...initArray];
 
    eventArray.forEach((element, ind )=> {         
        groupEvents.push(element);
        initDelList.push(element);
        eventArray.forEach((item, index) => {
                 condition =  ((item.startDate > element.startDate) && (item.startDate < element.endDate))
                                    || ((item.endDate > element.startDate) && (item.endDate < element.endDate));
            if(condition) {
                groupEvents.push(item);
                eventDelList.push(item);
            }            
        })
        this.removeFromArray(eventArray, eventDelList); 
        currentWidthPx = curColumn? 100 : 200;
        initLeftOffsetPx = curColumn? 101.5: 1.5;
        groupEvents.forEach((item,i) => {            
                if(i>0) return 0;
                var newStyle = {
                height: (Date.parse(item.endDate)-Date.parse(item.startDate))/1000/60*pxPermin + "px", 
                width: currentWidthPx+"px",
                transform: "translateY("+(Date.parse(item.startDate)-initDate)/1000/60*pxPermin+"px)", 
                left: (initLeftOffsetPx+currentWidthPx*curColumn)+"px", 
                position: "absolute"};
                showItems.push(<Event 
                key = {item._id} 
                style = {newStyle} 
                title = {item.title} 
                onClick = {() => this.props.onClick(item)} />);
        });
        groupEvents = [];
        eventDelList = [];        
    });
    this.removeFromArray(initArray, initDelList);
    initDelList = [];
    curColumn += 1;

}
    return showItems;
}


render() {
    console.log('event.render');
    let showItems = this.buildTaskGrip();

    return(
    <Fragment >
        {this.props.loading ? <Spinner /> : showItems} 
    </Fragment>
    );
    }   
}

const mapStateToProps = state => {
    return {
        appointments: state.event.appointments,
        loading: state.event.loading,
        calendar_id: state.calendar.id,
        user_id: state.user.id,
        token: state.user.token,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onGetList: (user_id, calendar_id, token) => dispatch(actions.getListRequest(user_id, calendar_id, token)),
        addEvent: (event, token) => dispatch(actions.addEvent(event, token)),
        updateEvent: (event,token) => dispatch(actions.updateEvent(event, token)),
        removeEvent: (event, token) => dispatch(actions.removeEvent(event, token)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Events);
