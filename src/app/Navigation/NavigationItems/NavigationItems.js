import React, { Fragment} from 'react';
import './navigationitems.css';
import NavigationItem from './NavigationItem/NavigationItem';


const navigationItems = () => (
    <Fragment>
     {/* <ul className={classes.NavigationItems}> */}
    <div className="menu-item">
        <NavigationItem link="/calendar" className="menu-ico schedule active menu-title active" exact>
            <div className="menu-ico schedule"></div>
			<div className="menu-title">Calendar</div>
        </NavigationItem>        
    </div>
    <div className="menu-item">
        <NavigationItem link="/logout"  className="menu-ico logout active menu-title active" exact>
            <div className="menu-ico logout"></div>
			<div className="menu-title">Logout</div>
        </NavigationItem>
    </div>
 {/* </ul> */}
    </Fragment>
);

export default navigationItems;